from flask import Flask, url_for
from cubes.server import slicer
from cubes.compat import ConfigParser

import json

app = Flask(__name__)
@app.route('/')
def hello_world():
    return 'Hello World1233!'

def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@app.route("/site-map")
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links.append((url, rule.endpoint))
    return json.dumps(links)

settings = ConfigParser()
settings.read("/root/src/project/slicer.ini")

app.register_blueprint(slicer, url_prefix="/slicer", config=settings)

if __name__ == "__main__":
    # Create a Slicer and register it at http://localhost:5000/slicer
    app.register_blueprint(slicer, url_prefix="/slicer", config="/root/src/project/slicer.ini")
    app.run(host = '0.0.0.0',
        port = 5000,  
        debug = True )