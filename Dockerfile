FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR /root/src

RUN pip install pika

RUN pip install PyMySQL

RUN pip install pysnowflake

RUN pip install redis

RUN pip install SQLAlchemy==1.2.1 -i https://mirrors.aliyun.com/pypi/simple

RUN pip install click

RUN pip install flask

RUN pip install uwsgi

RUN pip install cryptography

RUN pip install pyjwt

COPY . /root

RUN pwd

RUN git clone https://github.com/minshin/cubes.git
RUN /bin/sh -c 'cd ./cubes && python setup.py bdist_egg &&  easy_install ./dist/cubes-1.1-py3.6.egg'

CMD [ "uwsgi","config.ini" ]
